<?php get_header(); ?>

	<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

		<?php if (is_category()) { ?>
			<div class="archive-title">
				<span><?php _e( 'Posts categorized:', 'bonestheme' ); ?></span> <?php single_cat_title(); ?>
			</div>

		<?php } elseif (is_tag()) { ?>
			<div class="archive-title">
				<span><?php _e( 'Posts tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
			</div>

		<?php } elseif (is_author()) {
			global $post;
			$author_id = $post->post_author;
		?>
			<div class="archive-title">
				<span><?php _e( 'Posts by:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
			</div>
		<?php } elseif (is_day()) { ?>
			<div class="archive-title">
				<span><?php _e( 'Daily archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
			</div>

		<?php } elseif (is_month()) { ?>
			<div class="archive-title">
				<span><?php _e( 'Monthly archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
			</div>

		<?php } elseif (is_year()) { ?>
			<div class="archive-title">
				<span><?php _e( 'Yearly archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
			</div>
		<?php } ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part('excerpt'); ?>

		<?php endwhile; ?>

		<?php bones_page_navi(); ?>

		<?php else : ?>

			<?php get_template_part('not-found'); ?>

		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
