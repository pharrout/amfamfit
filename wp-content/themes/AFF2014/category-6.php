<?php get_header(); ?>

	<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">
		<div style="padding:20px;">
		<h2>Success Stories</h2>
		<p><em>There have been so many lives that have been changed through American Family Fitness and their personal trainers! Check out those stories below or <a href="https://amfamfitva.wufoo.com/forms/fitness-success-story/">submit your own success story</a> and have it featured online!</em></p>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<?php get_template_part('fss'); ?>

		<?php endwhile; ?>



		<?php else : ?>
			
			<?php get_template_part('not-found'); ?>

		<?php endif; ?>

	</div>
	</div>
	<?php get_sidebar(); ?>

<?php get_footer(); ?>
