<?php //start by fetching the terms for the animal_cat taxonomy
$terms = get_terms( 'gym', array(
    'hide_empty' => 0
) );
?>

<?php get_header(); ?>

	<div id="main" class="m-all t-2of3 d-7of7 cf" role="main">
		<img src = "http://amfamfit.com/wp-content/uploads/2015/05/PT_BIOS.jpg" alt = "Let's Get Personal" />
		<div style="padding:20px;">


		<?php
		// now run a query for each animal family
		foreach( $terms as $term ) {

		    // Define the query
		    $args = array(
		        'gym' => $term->slug
		    );
		    $query = new WP_Query( $args );

		    // output the term name in a heading tag
		    echo'<h2 style = "clear: both;">' . $term->name . '</h2>';

		    // output the post titles in a list
		    echo '<ul>';

		        // Start the Loop
		        while ( $query->have_posts() ) : $query->the_post(); ?>

		        <?php get_template_part('ptbio'); ?>

		        <?php endwhile;

		    echo '</ul>';

		    // use reset postdata to restore orginal query
		    wp_reset_postdata();

		} ?>

	</div>
	</div>

<?php get_footer(); ?>
