<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-excerpt cf' ); ?> role="article">

	<header class="article-header">
		<h3 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<p class="byline vcard"><time class="updated" datetime="<?php the_time('Y-m-j H:i:s'); ?>" pubdate><?php the_time('F jS, Y'); ?></time></p>
	</header>

	<section class="entry-content cf">
		<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
		<?php the_excerpt(); ?>
	</section>

</article>