<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-fss cf' ); ?> role="article" style="float:left;margin:0 0px; padding:0px; height:220px;">
	
	<section class="entry-content cf">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail("thumbnail"); ?></a>
		<p style="text-align:center;" class="entry-title"><strong><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></strong></p>
	</section>

</article>