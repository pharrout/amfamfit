<?php
// -----------------------------------------------------------------------------
// The social page. Three columns of stuff...?
//
// Template Name: Full Width
// -----------------------------------------------------------------------------
?>

<?php get_header(); ?>

	<div style="width:100%" id="main" class="full m-all t-2of3 d-5of7 cf" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article style="width:100%;" id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

			<?php if (has_excerpt()) : ?>
				<header class="article-header">
				<?php /* <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1> */ ?>
					<div class="page-excerpt">
						<?php the_excerpt(); ?>
					</div>
				</header>
			<?php endif; ?>

			<section style="width:100%;" class="entry-content cf" itemprop="articleBody">
				<?php the_content(); ?>
			</section>

		</article>

		<?php endwhile; else : ?>

			<?php get_template_part('not-found'); ?>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>
