<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<?php
			$ancestors = get_ancestors($post->ID, 'page');

			if(!in_array(325, $ancestors)) {
		?>
		<!-- <?php print_r(wp_get_post_parent_id($post->ID)); ?> -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<?php } ?>


		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo IMGDIR; ?>/apple-icon-touch.png">
		<link rel="shortcut icon" href="<?php echo IMGDIR; ?>/favicon.ico">

		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

		<script src="//assets.adobedtm.com/1f6b6f1b726a7760119f9a1ca483d8b73674f86b/satelliteLib-3f7937772777ac6041333dac36afbac314998c3b.js"></script>

	</head>

	<body <?php body_class(); ?>>

		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PBXTFH"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PBXTFH');</script>
		<!-- End Google Tag Manager -->

		<div id="container">

			<header class="header" role="banner">

				<div id="inner-header" class="wrap cf">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<a href="<?php echo home_url(); ?>/" rel="nofollow"><img id="logo" src="<?php echo IMGDIR; ?>/logo.png" alt="<?php bloginfo('name'); ?>"></a>

					<ul id="social" class="desktop-only">
						<li><small class="extraInfo">Make Payments, Update Account Info &amp; More!</small> <a class="members" href="https://www.memberselfservice.com/510203">Member sign-in</a></li>
						<li><a class="twitter" href="https://twitter.com/amfamfit">Twitter</a></li>
						<li><a class="faceboo" href="https://www.facebook.com/AmericanFamilyFitness">Facebook</a></li>
						<li style="display:none;"><a class="youtube" href="https://www.youtube.com/user/AmFamFitness">YouTube</a></li>
					</ul>


					<a id="menu-button" class="mobile-only" href="#show-menu">&#9776;</a>

					<nav role="navigation">
						<a href="<?php echo home_url(); ?>/"><img id="logo-words" src="<?php echo IMGDIR; ?>/logo-words.png" alt="<?php bloginfo('name'); ?>"></a>

						<?php wp_nav_menu(array(
							'container' => false,                           // remove container div
	    					'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
	    					'menu_class' => 'nav top-nav cf',               // adding custom nav class
	    					'theme_location' => 'main-nav',                 // where it's located in the theme
	    					'before' => '',                                 // before the menu
		        			'after' => '',                                  // after the menu
		        			'link_before' => '',                            // before each link
		        			'link_after' => '',                             // after each link
		        			'depth' => 0,                                   // limit the depth of the nav
	    					'fallback_cb' => ''                             // fallback function (if there is one)
						)); ?>
					</nav>

				</div>

			</header>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<?php if (is_singular() && has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('full'); ?>
					<?php endif; ?>

					<?php if (get_post_status(95) == 'private') : ?>
						<?php $info = get_post(95); # 95 = '[Breaking news/ticker/marquee thing]' ?>
						<div id="information"><?php echo wptexturize($info->post_content); ?></div>
					<?php endif; ?>