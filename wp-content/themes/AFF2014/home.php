<?php get_header(); ?>

	<?php $cycle = get_post(76); echo $cycle->post_content; # 76 = '[Home page cycle]' ?>

	<div id="main" class="m-all t-all d-all cf" role="main">

		<?php $columns = get_post(88); echo wptexturize($columns->post_content); # 88 = '[Home page columns]' ?>

	</div>

<?php get_footer(); ?>
