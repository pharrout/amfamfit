/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*//*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/function updateViewportDimensions(){var e=window,t=document,n=t.documentElement,r=t.getElementsByTagName("body")[0],i=e.innerWidth||n.clientWidth||r.clientWidth,s=e.innerHeight||n.clientHeight||r.clientHeight;return{width:i,height:s}}function loadGravatars(){viewport=updateViewportDimensions();viewport.width>=768&&jQuery(".comment img[data-gravatar]").each(function(){jQuery(this).attr("src",jQuery(this).attr("data-gravatar"))})}var viewport=updateViewportDimensions(),waitForFinalEvent=function(){var e={};return function(t,n,r){r||(r="Don't call this twice without a uniqueId");e[r]&&clearTimeout(e[r]);e[r]=setTimeout(t,n)}}(),timeToWaitForLast=100;jQuery(document).ready(function(e){var t=updateViewportDimensions();if(e(".accordion").length){function n(t){t.preventDefault();e(t.currentTarget).is("tr")?e(this).next().is(":visible")?e(this).next().fadeOut(250):e(this).next().fadeIn(250):e(this).next().is(":visible")?e(this).removeClass("open").next().slideUp(250):e(this).addClass("open").next().slideDown(250);return!1}e(".accordion:not(table) > *:odd").addClass("odd").css("display","none");e(".accordion:not(table) > *:even").addClass("even").click(n);e("table.accordion tr:odd").addClass("odd").css("display","none");e("table.accordion tr:even").addClass("even").click(n)}if(t.width<768){var r=e("#menu-button").next("nav");r.hide();e("#menu-button").click(function(e){e.preventDefault();r.is(":visible")?r.slideUp(250):r.slideDown(250);return!1})}e(window).load(function(){if(t.width>=768)if(e("#sidebar1").height()<e("#main").height()){e("#sidebar1").css("min-height",Math.round(e("#main").height())+"px");e(".one-week-pass img").css({bottom:0,position:"absolute"})}else e("#sidebar1").height()>e("#main").height()&&e("#main").css("min-height",Math.round(e("#sidebar1").height())+"px")})});