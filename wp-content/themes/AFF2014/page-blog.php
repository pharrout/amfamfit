<?php get_header(); ?>
<?php
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;

	$blogs = new WP_Query(array(
		'category_name' => 'blog',
		'posts_per_page' => get_option('posts_per_page'),
		'paged' => $paged
	));

	$pagination = paginate_links(array(
		'format' => '/page/%#%',
		'current' => $paged,
		'total' => $blogs->max_num_pages,
		'prev_text' => '&laquo; Newer',
		'next_text' => 'Older &raquo;'
	));
?>

	<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

		<?php if ($blogs->have_posts()) : ?>
			<?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
				<?php get_template_part('excerpt', 'blog'); ?>
			<?php endwhile; ?>

			<p class="pager"><?php echo $pagination; ?></p>
		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
