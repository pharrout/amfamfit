<?php # Template Name: Free Week ?>

<!doctype html>



<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->

<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->

<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->

<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



<head>

	<?php global $post; ?>

	<?php $fields = get_post_meta($post->ID); po_log($fields); ?>



	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title(''); ?></title>

	<meta name="HandheldFriendly" content="True">

	<meta name="MobileOptimized" content="320">

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200' rel='stylesheet' type='text/css'>

	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.ico">

	<script src="//assets.adobedtm.com/1f6b6f1b726a7760119f9a1ca483d8b73674f86b/satelliteLib-3f7937772777ac6041333dac36afbac314998c3b.js"></script>



	<?php if (array_key_exists('Additional CSS', $fields)) : ?>

		<style><?php echo $fields['Additional CSS'][0]; ?></style>

	<?php endif; ?>



<style>

	body { font-size:20px; font-weight: 400; font-family: 'Source Sans Pro', sans-serif; color:#fffefe; background: #000 url('<?php bloginfo('template_url'); ?>/library/images/body-bg.png') repeat-x left top; }

	.row { max-width:48em; margin:0 auto; width:100%; font-weight: 300; }

	.row::before, .row::after { content:" "; display:table; }

	.row::after { clear:both; }

	

	.columns,

	.column { float:left; }

	img { width:100%; max-width:100%; }

	.clear { clear:both; }

	.text-left { text-align:left; }

	p { margin-top:0; }

	

	.row .inner.text-left { text-align:left; }

	h1,h2,h3,h4,h5 { color:#fbec49; margin:0; }

	

	header {}

		header img { width:auto; }

		

		header a#logo { margin-left:50px; margin-bottom:5px; }

		

		header div.rounded-area {

			padding:8px 15px 2px 15px;

			background:#000;

			-webkit-border-top-left-radius: 8px;

			-webkit-border-top-right-radius: 8px;

			-moz-border-radius-topleft: 8px;

			-moz-border-radius-topright: 8px;

			border-top-left-radius: 8px;

			border-top-right-radius: 8px;

		}

		

		header div.rounded-area span.class-schedule { font-size:15px; line-height:18px; float:right; display:inline-block; padding-right:114px; padding-top:3px; position:relative; }

		header div.rounded-area span.class-schedule .bolder { font-weight:bold; }

		header div.rounded-area span.class-schedule a.gradient {

			display:inline-block; position:absolute; right:0; top:0; z-index:1; right:-8px; text-transform:uppercase; font-size:12px; font-weight:bold; text-decoration:none; color:white; padding:3px 10px 3px 10px;

			-webkit-border-radius: 3px;

			-moz-border-radius: 3px;

			border-radius: 3px;

			-webkit-box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.35);

			-moz-box-shadow:    0px 3px 2px 0px rgba(0, 0, 0, 0.35);

			box-shadow:         0px 3px 2px 0px rgba(0, 0, 0, 0.35);

			background: -moz-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* ff3.6+ */

			background: -webkit-gradient(linear, left top, right top, color-stop(0%, #75180d), color-stop(50%, #ae2009), color-stop(100%, #75180d)); /* safari4+,chrome */

			background: -webkit-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* safari5.1+,chrome10+ */

			background: -o-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* opera 11.10+ */

			background: -ms-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* ie10+ */

			background: linear-gradient(90deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* w3c */

			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#75180d', endColorstr='#75180d',GradientType=1 ); /* ie6-9 */

		}

		

		header div.social-media { float:right; }

		header div.social-media a { display:inline-block; width:43px; height:31px; text-indent:-999em; background:transparent url('<?php bloginfo('template_url'); ?>/library/images/socials.png') no-repeat; }

		header div.social-media a.facebook { width:16px; margin-left:15px; background-position:-56px 0; }

	

	div#content { max-width:48em; margin:0 auto; background:#404040 url('<?php bloginfo('template_url'); ?>/library/images/grey-pattern.jpg') repeat; }

	

		.row .inner { padding-left:5%; padding-right:5%; text-align:center; }

		.row .inner h1 { margin-bottom:20px; }

		.row .text-block h2 { margin-bottom:15px; }

		

			div#slider { margin-bottom:20px; }

			

			div.intro.what-do-i-get ul > li { padding-bottom:5px; }

		

			div.schedule { position:relative; z-index:1; margin-bottom:30px; padding:20px; text-align:center }

			div.schedule:before { content:""; position:absolute; left:0; top:0; z-index:-1; width:100%; height:100%; background:#000; opacity:0.25; filter:alpha(opacity=25); }

				div.schedule h2 { font-size:22px; line-height:22px; margin:0 0 15px 0; }

				div.schedule a { display:inline-block; text-decoration:none; color:white; position:relative; margin:0 10px 10px 10px; padding:5px 20px; width:20%;

					-webkit-border-radius: 3px;

					-moz-border-radius: 3px;

					border-radius: 3px;

					background: -moz-linear-gradient(0deg, #083370 0%, #32588c 50%, #083370 100%); /* ff3.6+ */

					background: -webkit-gradient(linear, left top, right top, color-stop(0%, #083370), color-stop(50%, #32588c), color-stop(100%, #083370)); /* safari4+,chrome */

					background: -webkit-linear-gradient(0deg, #083370 0%, #32588c 50%, #083370 100%); /* safari5.1+,chrome10+ */

					background: -o-linear-gradient(0deg, #083370 0%, #32588c 50%, #083370 100%); /* opera 11.10+ */

					background: -ms-linear-gradient(0deg, #083370 0%, #32588c 50%, #083370 100%); /* ie10+ */

					background: linear-gradient(90deg, #083370 0%, #32588c 50%, #083370 100%); /* w3c */

					filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#083370', endColorstr='#083370',GradientType=1 ); /* ie6-9 */

					-webkit-box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.35);

					-moz-box-shadow:    0px 3px 2px 0px rgba(0, 0, 0, 0.35);

					box-shadow:         0px 3px 2px 0px rgba(0, 0, 0, 0.35);

				}

				

			div.locations { position:relative; z-index:1; margin-bottom:30px; }

			div.locations:before { background:#000; opacity:0.65; filter:alpha(opacity=65); width:100%; height:100%; position:absolute; left:0; top:0; content:""; z-index:-1; }

				div.locations .columns { width:50%; }

				div.locations .map { border:3px solid #000; width:48%; }

			div.locations:after, div.locations::after { clear:both; }

			

			div.outside-H2 h2 { margin-bottom:15px; }

			

			div.locations .branch:first-child { margin-top:15px; }

			div.locations .branch:last-child { margin-bottom:0px; }

			div.locations .branch { font-size:16px; text-align:left; margin-bottom:15px; padding-left:20px; padding-right:150px; position:relative; }

				div.locations .branch h3 { font-size:16px; }

				div.locations .branch a.map-it { display:inline-block; position:absolute; right:0; top:0; z-index:1; text-decoration:none; color:white; padding:5px 40px 5px 20px;

					-webkit-border-radius: 3px;

					-moz-border-radius: 3px;

					border-radius: 3px;

					-webkit-box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.35);

					-moz-box-shadow:    0px 3px 2px 0px rgba(0, 0, 0, 0.35);

					box-shadow:         0px 3px 2px 0px rgba(0, 0, 0, 0.35);

					background: -moz-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* ff3.6+ */

					background: -webkit-gradient(linear, left top, right top, color-stop(0%, #75180d), color-stop(50%, #ae2009), color-stop(100%, #75180d)); /* safari4+,chrome */

					background: -webkit-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* safari5.1+,chrome10+ */

					background: -o-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* opera 11.10+ */

					background: -ms-linear-gradient(0deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* ie10+ */

					background: linear-gradient(90deg, #75180d 0%, #ae2009 50%, #75180d 100%); /* w3c */

					filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#75180d', endColorstr='#75180d',GradientType=1 ); /* ie6-9 */

				}

				

				div.locations .branch a.map-it:after { position:absolute; content:""; top:9px; right:15px; z-index:-1;

					width: 0;

					height: 0;

					border-style: solid;

					border-width: 6.5px 0 6.5px 8px;

					border-color: transparent transparent transparent #ffffff;

				}

				

			div.testimonials { text-align:left; }

				div.testimonials span { display:block; color:#fbec49; }

				

			div.what-do-i-get {}

				div.what-do-i-get h2 { margin-bottom:15px; }

				div.what-do-i-get ul { margin:0; padding:0; list-style:none; }

					div.what-do-i-get ul > li { position:relative; padding-left:15px; padding-bottom:20px; }

					div.what-do-i-get ul > li:before { content:"\002022"; display:inline-block; color:#fbec49; position:absolute; left:0; top:5px; font-size:30px; line-height:13px; }

			

	footer { padding-top:20px; }

	footer .columns { width:50%; text-align:left; }

	footer .columns img { width:auto; }

	footer .columns:last-child { text-align:right; text-transform:uppercase; font-size:14px; padding-top:50px; }

	

	/* wufoo Form */

	.wufoo label.choice,

	.wufoo label.desc,

	.wufoo legend.desc,

	.wufoo li div,

	.wufoo li span,

	.wufoo li div label,

	.wufoo li span label { color:white !important; font-family:"Source Sans Pro",sans-serif !important; font-size:20px !important; }

	

	div#wufoo-meonj120cewy76 { position:relative; z-index:1; margin-bottom:40px; padding:20px; }

	div#wufoo-meonj120cewy76:before { content:""; position:absolute; left:0; top:0; z-index:-1; width:100%; height:100%; background:#fff; opacity:0.5; filter:alpha(opacity=50); }

	/* end wufoo Form */

	

	@media (max-width:915px) {

		header div.rounded-area span.class-schedule { font-size:12px; line-height:14px; }

	}

	

	@media (max-width:824px) {

		header div.rounded-area span.class-schedule { font-size:12px; line-height:13px; width:35%; text-align:center; padding-top:0; }

	}

	

	@media (max-width:815px) {

		div.schedule a { width:24%; padding-left:10px; padding-right:10px; }

	}

	

	@media (max-width:768px) {

		header div.rounded-area span.class-schedule { width:28%; }

	}

	

	@media (max-width:700px) {

		div.schedule a { margin-left:5px; margin-right:5px; width:26%; }

		div.locations .branch { padding-right:110px; }

	}

	

	@media (max-width:640px) {

		div.schedule a { margin-left:5px; margin-right:5px; width:60%; }

		

		div.locations .columns { width:100%; }

		div.locations .branch a.map-it { right:10px; }

		div.locations .branch:last-child { margin-bottom:15px; }

		

		header div.rounded-area span.class-schedule {

		 	font-size: 14px;

			line-height: 20px;

			margin-top: 15px;

			padding-bottom: 10px;

			text-align: right;

			width: 100%;

		}

	}

	

	@media (max-width:548px) {

		header div.rounded-area span.class-schedule { font-size:12px; }

	}

	

	@media (max-width:480px) {

		header .main-menu-area { text-align:center; }

		header a#logo { margin-left:0; }

		header div.social-media { float:none; position:absolute; top:5px; right:10px; }

		header div.rounded-area { text-align:center; }
		header div.rounded-area span.class-schedule { padding-right:0; text-align:center; }
		header div.rounded-area span.class-schedule a.gradient { display:block; margin-top:10px; margin-right:10%; margin-left:10%; position:relative; right:auto; left:0; top:0; bottom:auto; }

		footer .columns:first-child { width:25%; }

		footer .columns:last-child { width:75%; }

	}

</style>

</head>



<body>

	<!-- Google Tag Manager -->

		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PBXTFH"

		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

		})(window,document,'script','dataLayer','GTM-PBXTFH');</script>

		<!-- End Google Tag Manager -->

    <header>

    	<div class="row">

            <div class="main-menu-area">

                <a href="/" id="logo"><img src="<?php bloginfo('template_url'); ?>/library/images/logo.png" alt="American Family Fitness" /></a>

                

                <div class="social-media">

                    <a href="https://twitter.com/amfamfit" class="twitter" target="_blank">Twitter</a>

                    <a href="https://www.facebook.com/AmericanFamilyFitness" class="facebook" target="_blank">Facebook</a>

                </div>

            </div>

            <div class="rounded-area">

                <a href="/"><img src="<?php bloginfo('template_url'); ?>/library/images/logo-words.png" alt="American Family Fitness" /></a>

                <span class="class-schedule"><font class="bolder">Existing Member?</font> Don't miss the intense dance fitness of "Groove" <a href="<?php bloginfo('url'); ?>/schedules-2/" class="gradient">Class Schedule</a></span>

            	<div class="clear"></div>

            </div>

        </div><!-- .row -->

    </header>

    

    <div class="row" id="content">

    	<div id="slider">

            <a href="#form-pass"><img src="<?php echo site_url(); ?>/wp-content/uploads/2016/03/free-pass-mar16-edit2.jpg" alt="FREE WEEK" /></a>

        </div>



        

        <div class="inner">

        	<div class="intro what-do-i-get text-left">

            	<h1>Everything You Want In A Gym & Then Some</h1>

                <p>

                	<ul>

                    	<li>Nine gym locations in Richmond, Fredericksburg and Williamsburg</li>

                    	<li>24 hour access Sunday - Friday at most locations</li>

                    	<li>Memberships starting as low as $39.95 a month</li>

                    	<li>100's of Group X classes each week, personal training, modern equipment, pools, court sports, child care and more</li>

                    </ul>

                </p>

                <p><font style="color:#fbec49">PLUS:</font> New members receive a free 1 hour functional movement screening with a certified personal trainer to determine strengths and weaknesses so that they can recommend a personalized work out that’s best for you.</p>

        	</div>

        </div><!-- .row -->

        

        <?php /*?><div class="inner">

        	<div class="schedule">

            	<h2>View Class Schedules for:</h2>

                <a href="<?php bloginfo('url'); ?>/schedules-2/chester/schedules/" class="blue">Chester</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-mechanicsville/mv-group-fitness/" class="blue">Mechanicsville</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-west-end/sp-group-fitness/" class="blue">Short Pump</a>

                

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-colonial-heights/" class="blue">Colonial Heights</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-midlothian/md-group-fitness/" class="blue">Midlothian</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-virginia-center-commons/vcc-group-fitness/" class="blue">VA Ctr Commons</a>

                

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-fredericksburg/fb-group-fitness/" class="blue">Fredericksburg</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-swift-creek/sc-group-fitness/" class="blue">Swift Creek</a>

                <a href="<?php bloginfo('url'); ?>/schedules-2/schedules-williamsburg/" class="blue">Williamsburg</a>

            </div>

        </div><!-- .row --><?php */?>

        
        <div class="inner text-left">

        	<div class="obligation text-block" id="form-pass">

            	<h2>Get Your Free Pass!</h2>

                <p>Fill out the form below to receive your free pass.</p>

            </div>

        </div><!-- .row -->

        

        <div class="inner text-left">

        	<div id="wufoo-meonj120cewy76">Fill out my <a href="https://pharroutrva.wufoo.com/forms/meonj120cewy76">online form</a>.</div>

        	<div class="form">

            	<script type="text/javascript">// <![CDATA[

					var meonj120cewy76;(function(d, t) {

					var s = d.createElement(t), options = {

					'userName':'pharroutrva',

					'formHash':'meonj120cewy76',

					'autoResize':true,

					'height':'832',

					'async':true,

					'host':'wufoo.com',

					'header':'show',

					'ssl':true};

					s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';

					s.onload = s.onreadystatechange = function() {

					var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;

					try { meonj120cewy76 = new WufooForm();meonj120cewy76.initialize(options);meonj120cewy76.display(); } catch (e) {}};

					var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);

					})(document, 'script');

					// ]]></script>

            </div>

        </div>

        

        <div class="inner text-left outside-H2">

        	<h2>Find a location convenient for you</h2>

        	<div class="locations">

            	<div class="columns map">

                	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d405380.6224828823!2d-77.48162789999999!3d37.45986925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1smaps+-+american+family+fitness+branches+in+richmond!5e0!3m2!1sen!2sph!4v1438880407131" width="100%" height="515px" frameborder="0" style="border:0" allowfullscreen></iframe>

                </div>

                <div class="columns areas">

                

                	<div class="branch">

                    	<h3>Chester</h3>

                        <span>12201 S Chalkley Rd, 23831</span>

                        

                        <a href="https://goo.gl/maps/U8koP" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>Colonial Heights</h3>

                        <span>501 E Roslyn Rd, 23834</span>

                        

                        <a href="https://goo.gl/maps/Q7Iw7" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>Fredericksburg</h3>

                        <span>10020 Southpoint Pkwy, 22407</span>

                        

                        <a href="https://goo.gl/maps/oiPHT" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>Mechanicsville</h3>

                        <span>6337 Mechanicsville Tnpk, 23111</span>

                        

                        <a href="https://goo.gl/maps/WB9wF" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>Midlothian</h3>

                        <span>12900 Amfit Way, 23114</span>

                        

                        <a href="https://goo.gl/maps/KGGHi" target="_blank" class="map-it">Map it</a>

                    </div>



					<div class="branch">

                    	<h3>Swift Creek</h3>

                        <span>4751 Brad McNeer Pkwy, 23112</span>

                        

                        <a href="https://goo.gl/maps/MZzJh" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>Short Pump</h3>

                        <span>11760 W Broad St, 23233</span>

                        

                        <a href="https://goo.gl/maps/bkPbh" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                    <div class="branch">

                    	<h3>VA Ctr Commons</h3>

                        <span>10101 Brook Rd. Ste 100, 23059</span>

                        

                        <a href="https://goo.gl/maps/ckgG6" target="_blank" class="map-it">Map it</a>

                    </div>



					<div class="branch">

                    	<h3>Williamsburg</h3>

                        <span>5137 Main St, Williamsburg, VA 23188</span>

                        

                        <a href="https://goo.gl/maps/8aUhF" target="_blank" class="map-it">Map it</a>

                    </div>

                    

                </div>

                

                <div class="clear"></div>

            </div>

        </div><!-- .row -->

        

        <div class="inner text-left">

        	<div class="testimonials text-block">

				<p><a style="color:#fbec49;" href="http://amfamfit.com/about-us/guidelines/">Guest Policy</a></p>

            	<h2>What Do Other Members Love About AFF?</h2>

                <p><i>"The staff at American Family made me feel like I could reach my weight loss goals and walked (baby steps) with me from the very beginning."</i><span class="client">- Sharon S.</span></p>

            	<p><i>"My kids love the KidZone so much. My two year old sometimes cries when we have to leave."</i><span class="client">- Jonell G.</span></p>

                <p><i>"I have struggled with my weight all of my life. The staff at American Family helped give me the confidence and accountability that I needed to stick to it. I am doing now things that before would have been impossible."</i><span class="client">- Tim C.</span></p>

            </div>

        </div><!-- .row -->

        

        <div class="inner text-left">

        	<div class="what-do-i-get text-block">

            	<h2>What Will I Get With A Membership?</h2>

                <div class="list">

                	<ul>

                    	<li>Free 8-week course designed to teach you the basics of exercise and familiarize you with the many types of fitness we have to offer.</li>

						<li>Access to cardio equipment, machine weights, free weights, group exercise classes, cycling, basketball, racquetball, pool access and water fitness classes where available and more.</li>

                    	<li>Access to the KidZone and kid activities (available with family membership)</li>

                    	<li>Hot Yoga at our VCC location. Yoga and Pilates at all locations.</li>

                    	<li>Access to sauna, steam room, hot tub and pools (where available).</li>

                    	<li>Special days to bring a friend for free.</li>

                    	<li>Opportunity to serve the community through our community outreach programs.</li>

                    	<li>Games and contests to keep exercising fun!</li>

                    </ul>

                </div>

            </div>

        </div><!-- .row -->

        

        <div class="inner text-left">

        	<div class="what-do-i-do text-block">

            	<h2>What Do I Do If I Decide To Join After My Free Visit?</h2>

                <p>Stop by the front desk and tell them you'd like to discuss joining AFF. Next you will meet with a Membership Director that will review all of the memberships that we offer and help you determine which is best for you.</p>

            </div>

        </div><!-- .row -->

        

         <div class="inner text-left">

        	<div class="obligation text-block">

            	<h2>Is There Any Obligation?</h2>

                <p>There is no obligation to join AFF after your free visit.</p>

            </div>

        </div><!-- .row -->


    </div>

    

    <footer>

    	<div class="row">

        	<div class="columns">

            	<a href="http://www.bbb.org/richmond/business-reviews/health-clubs/american-family-fitness-in-glen-allen-va-4017950/#bbbonlineclick">

                	<img src="<?php bloginfo('template_url'); ?>/library/images/bbb.png" alt="BBB" />

                </a>

            </div>

            <div class="columns copyright">

            	&copy; American Family Fitness | 2015

            </div>

        </div><!-- .row -->

    </footer>

	

    <script type="text/javascript">_satellite.pageBottom();</script>

    

</body>

</html>