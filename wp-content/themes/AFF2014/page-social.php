<?php
// -----------------------------------------------------------------------------
// The social page. Three columns of stuff...?
//
// Template Name: Social Page
// -----------------------------------------------------------------------------
?>

<?php get_header(); ?>
<?php
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;

	$blogs = new WP_Query(array(
		'category_name' => 'blog',
		'posts_per_page' => get_option('posts_per_page'),
		'paged' => $paged
	));

	$pagination = paginate_links(array(
		'format' => '/page/%#%',
		'current' => $paged,
		'total' => $blogs->max_num_pages,
		'prev_text' => '&laquo; Newer',
		'next_text' => 'Older &raquo;'
	));
?>

	<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">
	
	<div id = "topper">
		<div class = "columns columns-2 wrap">
			<div class = "column column-1 beAFan">
				<h3>Become a fan today</h3>
				<div style="padding-left:30px;">
					<p>From Twitter to Foursquare, there are all sorts of ways to connect with us online. Be a fan on the main AFF and your specific location!</p>
					<iframe src="http://www.facebook.com/plugins/like.php?app_id=128066013953563&amp;href=https%3A%2F%2Fwww.facebook.com%2FAmericanFamilyFitness&amp;send=false&amp;layout=standard&amp;width=375&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:375px; height:80px;" allowTransparency="true"></iframe>
			
				</div>
			</div>
			<div class = "column column-2">
				 <div class = "topper-title wrap">
					<a href = "https://www.facebook.com/AmericanFamilyFitness">
						<img src = "<?php bloginfo('template_url'); ?>/images/icons/facebook-48.png" alt = "Facebook">
					</a>
				 	<a href = "http://twitter.com/amfamfit">
						<img src = "<?php bloginfo('template_url'); ?>/images/icons/twitter-48.png" alt = "Twitter">
					</a>
				 	<a href = "http://www.youtube.com/user/AmFamFitness">
						<img src = "<?php bloginfo('template_url'); ?>/images/icons/youtube-48.png" alt = "YouTube">
					</a>
					<h3>Be social.<br>Connect online.</h3>
					
				</div>
				<div id = "update">
					<strong>DON'T FORGET TO CHECK-IN ON</strong> <a href = "https://foursquare.com/search?q=American+Family+Fitness&near=Richmond"> <img style="margin-left:5px;" src = "<?php bloginfo('template_url'); ?>/images/icons/foursquare.png" valign=middle alt = "Twitter"></a>
				</div>
			</div>
		</div>
	</div>
	
	<div id = "content">
		
		<?php if (have_posts()) : ?>
			<?php the_post(); ?>
			
			<div <?php post_class('wrap'); ?>>
				<div class = "entry-content">
					<h1 class = "entry-title"><?php the_title(); ?></h1>
					<?php the_content('Read more...'); ?>
					
					<div class = "columns columns-3 wrap">
						<div id = "facebook" class = "column column-1" style="text-align:center;">
							<a href = "#">
								<img class = "align-center" src = "<?php bloginfo('template_url'); ?>/images/icons/facebook-128.png" alt = "facebook">
							</a>
							<h3><strong>Are you a fan?</strong><br />
								Friend your location today!</h3>
								<?php if($currentgym){ ?>
									
									<?php echo gymfb($currentgym); ?>
								
								<?php } else { ?>
								<iframe src="http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FAmerican-Family-Fitness%2F221179469902&amp;width=250&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23ddd&amp;stream=true&amp;header=false&amp;height=500" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:600px;" allowTransparency="true"></iframe>
								<?php } ?>
						</div>
						<div id = "twitter" class = "column column-2">
							<a href = "http://twitter.com/amfamfit">
								<img class = "align-center" src = "<?php bloginfo('template_url'); ?>/images/icons/twitter-128.png" alt = "twitter">
							</a>
							<h3><strong>Follow Us</strong><br />Get all the latest updates!</h3>
								<?php echo po_get_tweets('amfamfit', 10); ?>	
						</div>
						<div id = "youtube" class = "column column-3" style="text-align:center;">
							<a href = "http://www.youtube.com/user/AmFamFitness">
								<img class = "align-center" src = "<?php bloginfo('template_url'); ?>/images/icons/youtube-128.png" alt = "youtube">
							</a>
								<h3><strong>SUBSCRIBE TODAY!</strong><br>
									Commericals, training, and tutorials!</h3>
							<br />
							<iframe width="250" height="172" src="http://www.youtube.com/embed/yrT2r-ZPy7w" frameborder="0" allowfullscreen></iframe>
							<iframe width="250" height="172" src="http://www.youtube.com/embed/R9kCeze9Xr4" frameborder="0" allowfullscreen></iframe>
							<iframe width="250" height="172" src="http://www.youtube.com/embed/nmqo7E1bZDI" frameborder="0" allowfullscreen></iframe>
							<iframe width="250" height="172" src="http://www.youtube.com/embed/LjbrkXf9NWo" frameborder="0" allowfullscreen></iframe>
							
						</div>
					</div>
					
				</div>
			</div>
			
		<?php else : ?>
			<?php get_template_part('/partials/404'); ?>
		<?php endif; ?>
		
	</div>
</div>

<?php require_once(TEMPLATEPATH . '/partials/footer.php'); ?>