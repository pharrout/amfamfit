<?php # Template Name: Splash Page ?>
<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<?php global $post; ?>
	<?php $fields = get_post_meta($post->ID); po_log($fields); ?>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php wp_title(''); ?></title>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php /* <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/library/css/style.css"> */ ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/page-splash.css">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.ico">
	<script src="//assets.adobedtm.com/1f6b6f1b726a7760119f9a1ca483d8b73674f86b/satelliteLib-3f7937772777ac6041333dac36afbac314998c3b.js"></script>

	<?php if (array_key_exists('Additional CSS', $fields)) : ?>
		<style><?php echo $fields['Additional CSS'][0]; ?></style>
	<?php endif; ?>

	<?php #wp_head(); ?>
</head>

<body <?php body_class('splash-page'); ?>>
	<div id="splash">

		<header class="header" role="banner">
			<div id="inner-header" class="wrap cf">

				<a href="<?php bloginfo('url'); ?>/" rel="nofollow"><img id="logo" src="<?php bloginfo('template_url'); ?>/library/images/logo.png" alt="AFF logo"></a><br>
				<a href="<?php bloginfo('url'); ?>/" rel="nofollow"><img id="logo-words" src="<?php bloginfo('template_url'); ?>/library/images/logo-words.png" alt="American Family Fitness"></a><br>

			</div>
		</header>

		<div id="content">
			<div id="inner-content" class="wrap cf">

				<?php if (has_post_thumbnail()) : ?>
					<?php if (array_key_exists('Image link', $fields)) : ?>
						<a href="<?php echo $fields['Image link'][0]; ?>"><?php the_post_thumbnail('full'); ?></a>
					<?php else : ?>
						<?php the_post_thumbnail('full'); ?>
					<?php endif; ?>
				<?php endif; ?>

				<?php if (get_post_field('post_content', $post->ID)) : ?>
					<?php echo wpautop(wptexturize(get_post_field('post_content', $post->ID))); ?>
				<?php endif; ?>

			</div>
		</div>

		<footer class="footer" role="contentinfo">
			<div id="inner-footer" class="wrap cf">

				<table id="links">
					<tr>
						<td id="locations">
							<a href="http://amfamfit.com/contact-us/#locations">To take advantage of this offer,<br>call or come by one of our locations.</a>
						</td>
						<td id="join">
							<a href="http://amfamfit.com/membership/free-oneweek-pass/">Try us out for a week free.</a>
						</td>
					</tr>
				</table>

				<a href="http://www.bbb.org/richmond/business-reviews/health-clubs/american-family-fitness-in-glen-allen-va-4017950/#bbbonlineclick">
					<img src="<?php bloginfo('template_url'); ?>/library/images/bbb.png" alt="BBB.org">
				</a>

				<p class="source-org copyright">&copy; <?php bloginfo( 'name' ); ?> | <?php echo date('Y'); ?></p>
			</div>
		</footer>

	</div>

	<?php #wp_footer(); ?>
	<script type="text/javascript">_satellite.pageBottom();</script>

	</body>
</html>