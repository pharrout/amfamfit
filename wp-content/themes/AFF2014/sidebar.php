				<div id="sidebar1" class="sidebar m-all t-1of3 d-2of7 cf" role="complementary">

				<?php
					if (is_singular() && po_has_sidebar_content()) {
						$sidebar = po_get_sidebar_content();
					} else {
						$sidebar = get_post(22); # 22 = '[Sidebar]'
						$sidebar = $sidebar->post_content;
					}

					echo wptexturize(wpautop($sidebar));
				?>

				</div>
