<?php get_header(); ?>

	<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

				<header class="article-header">

					<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>

				</header>

				<section class="entry-content cf" itemprop="articleBody">
					<?php
						if(in_category('9')) { // for PT bios
							if ( has_post_thumbnail() ) { the_post_thumbnail('full', array('class' => 'alignleft')); }
						}
					?>
					<?php the_content(); ?>
				</section>

				<?php #comments_template(); ?>

			</article>

		<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part('not-found'); ?>

		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
